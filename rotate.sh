#!/bin/sh
#
# Create a rotating figure using Distort SRT transformations

width=`identify -ping -format '%[width]' "$1"`
height=`identify -ping -format '%[height]' "$1"`
if [ "$width" -lt "$height" ]; then min="$width"; else min="$height"; fi
minh=$(($min / 2))

convert "$1" -crop $min\x$min+0+0 \( +clone -threshold -1 -negate -fill white -draw "circle $minh,$minh $minh,0" \) -alpha off -compose copy_opacity -composite "$1"

command='convert -delay 4 "$1" -virtual-pixel transparent'

for i in `seq 0 20 340`; do
  command="$command \\( -clone 0 -distort SRT $i \\)"
done

command=$command' -delete 0 -loop 0 -layers OptimizePlus "$2"'

eval $command
