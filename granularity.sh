#!/bin/sh

command='convert -delay 8 "$1" -virtual-pixel tile'

b=0.5
for i in `seq 16`;  do
  command="$command \\( -clone 0 -blur 0x$b \\)"
  b=`convert null: -format "%[fx: $b * 1.3 ]" info:`
done

command="$command  -ordered-dither threshold,3"
command="$command \\( -clone -2-1 \\)"
command="$command -loop 0 $2"

eval $command
