#!/bin/sh

for i in {1..10}
do
	../magik/crystallize.sh "$1" "$1-temp-$i.png"
done

convert -delay 8 "$1-temp-*.png" -loop 0 -layers OptimizePlus "$2"
