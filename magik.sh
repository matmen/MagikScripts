#!/bin/sh

convert "$1" -resize "512x512>" "$1"
resolution=`identify -ping -format '%[width]x%[height]' "$1"`
command='convert -delay 10 "$1"'

for i in `seq 100 -10 1`; do
  command="$command \\( -clone 0 -liquid-rescale $i% -liquid-rescale $((100 / $i * 75))% -resize $resolution! \\)"
done

command="$command -loop 0 -layers OptimizePlus $2"

eval $command
