#!/bin/sh

command='convert -delay 8 "$1" -alpha off'

for i in `seq 0.5 0.5 5`; do
  command="$command \\( -clone 0 -evaluate sine $i \\)"
done

command=$command' \( -clone -2-1 \) -loop 0 -layers OptimizePlus "$2"'

eval $command
