#!/bin/sh
#
# Create a animation of swirling mixer
# Source: https://www.imagemagick.org/Usage/warping/animate_mixer

command='convert -delay 8 "$1"'

for i in `seq 40 40 360; seq 320 -40 -360; seq -340 40 -40`; do
  command="$command \\( -clone 0 -swirl $i \\)"
done

command=$command' -loop 0 -layers OptimizePlus "$2"'

eval $command
