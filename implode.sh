#!/bin/sh
#
# Create a animation of an explosion,
# Setting the central color using a single pixel.
# This  color starts white and fades to black.
# (ASIDE: on a larger display, debris should also fade out).
# Source: https://www.imagemagick.org/Usage/warping/animate_explode

command='convert -delay 10 "$1"'

for i in `seq 0.1 0.1 2`; do
  command="$command \\( -clone 0 -implode $i -set delay 10 \\)"
done

command=$command' \( -clone -2-1 \) +repage -loop 0 -layers OptimizePlus "$2"'

eval $command
