#!/bin/sh

command='convert -delay 8 "$1"'

for i in `seq 20 20 340`; do
  command="$command \\( -clone 0 -colorspace sRGB -channel R -evaluate sine $i \\)"
done

command=$command' -loop 0 -layers OptimizePlus "$2"'

eval $command
